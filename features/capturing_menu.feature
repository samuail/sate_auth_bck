@menu
Feature: Capturing Menu Information.
  As a system user
  I want to capture menu information
  So that user can be able to perform activities related to the system

  Background:
    Given I am logged in as a super user
    And I have the following application module information
      | application_name | code |
      | Budget & Finance |  BF  |


  @create
  Scenario: Adding the menu information
    When I record a menu information with "System Setting" as text "xxx" as icon "abc.xyz" as class name "xyz" as location and "" as parent to be used on "Budget & Finance" application module
    Then I should have this menu information
      |       text     | icon_cls |  class_name  | location | parent | application_name |
      | System Setting |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |
    And I should see a success message "Menu was successfully created."

  @create_with_null_error_message
  Scenario: Adding the menu information
    When I record a menu information with "" as text "xxx" as icon "abc.xyz" as class name "xyz" as location and "" as parent to be used on "Budget & Finance" application module
    Then I should see an error message "Menu Text can't be blank"

  @create_with_duplicate_error_message
  Scenario: Adding the menu information
    And I have the following menu information
      |       text     | icon_cls |  class_name  | location | parent | application_name |
      | System Setting |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |
    When I record a menu information with "System Setting" as text "xxx" as icon "abc.xyz" as class name "xyz" as location and "" as parent to be used on "Budget & Finance" application module
    Then I should see an error message "Menu Text has already been taken"

  @update
  Scenario: Editing the menu information
    And I have the following menu information
      |       text     | icon_cls |  class_name  | location | parent | application_name |
      | System Setting |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |
    When I change the menu information with "Business Setting" as text "xxx" as icon "abc.xyz" as class name "xyz" as location and "" as parent to be used on "Budget & Finance" application module
    Then I should have this menu information
      |       text       | icon_cls |  class_name  | location | parent | application_name |
      | Business Setting |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |
    And I should see a success message "Menu was successfully updated."

  @update_with_null_error_message
  Scenario: Editing the menu information
    And I have the following menu information
      |       text     | icon_cls |  class_name  | location | parent | application_name |
      | System Setting |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |
    When I change the menu information with "" as text "xxx" as icon "abc.xyz" as class name "xyz" as location and "" as parent to be used on "Budget & Finance" application module
    Then I should see an error message "Menu Text can't be blank"

  @update_with_duplicate_error_message
  Scenario: Editing the menu information
    And I have the following menu information
      |       text     | icon_cls |  class_name  | location | parent | application_name |
      | System Setting |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |
    And I have the following menu information
      |       text       | icon_cls |  class_name  | location | parent | application_name |
      | Business Setting |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |
    When I change the menu information with "System Setting" as text "xxx" as icon "abc.xyz" as class name "xyz" as location and "" as parent to be used on "Budget & Finance" application module
    Then I should see an error message "Menu Text has already been taken"

  @list_all_user_roles
  Scenario: List all menu information
    And I have the following menu information
      |       text     | icon_cls |  class_name  | location | parent | application_name |
      | System Setting |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |
    And I have the following menu information
      |       text       | icon_cls |  class_name  | location | parent | application_name |
      | Business Setting |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |
    When I want to see all menu information
    Then I should have the following "2" menu informations
      |       text       | icon_cls |  class_name  | location | parent | application_name |
      | System Setting   |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |
      | Business Setting |   xxx    |   abc.xyz    |   xyz    |   nil  | Budget & Finance |