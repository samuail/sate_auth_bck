Feature: Logout the User from the application.
  As a system user
  I want to logout of the system
  So that i can no more access the system activities

  Background:
    Given I have the following application module information
      | application_name | code |
      | Budget & Finance | DUY  |
    And I have the following user information
      | first_name | last_name |    email    |  password  | application_name |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Budget & Finance |

  @logout_user
  Scenario: Logout user from the application
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to logout of the system
    Then I should be logged out of the system and see the message "User Successfully logged out."
