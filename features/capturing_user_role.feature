@user_role
Feature: Capturing User role Information.
  As a system user
  I want to capture user role information
  So that user can be able to perform activities related to the system

  Background:
    Given I am logged in as a super user
    And I have the following application module information
      | application_name | code |
      | Budget & Finance |  BF  |


  @create
  Scenario: Adding the user role information
    When I record a user role with the name "Administrator" to work on "Budget & Finance" application module
    Then I should have this user role information
      |      name     |
      | Administrator |
    And I should see a success message "User role was successfully created."

  @create_with_null_error_message
  Scenario: Adding the user role information
    When I record a user role with the name "" to work on "Budget & Finance" application module
    Then I should see an error message "User Role Name can't be blank"

  @create_with_duplicate_error_message
  Scenario: Adding the user role information
    And I have the following user role information
      |      name     | application_name |
      | Administrator | Budget & Finance |
    When I record a user role with the name "Administrator" to work on "Budget & Finance" application module
    Then I should see an error message "User Role Name has already been taken"

  @update
  Scenario: Editing the user role information
    And I have the following user role information
      |      name     | application_name |
      | Administrator | Budget & Finance |
    When I change the user roles name by "Super User" to work on "Budget & Finance" application module
    Then I should have this user role information
      |    name     | application_name |
      |  Super User | Budget & Finance |
    And I should see a success message "User role was successfully updated."

  @update_with_null_error_message
  Scenario: Editing the user role information
    And I have the following user role information
      |      name     | application_name |
      | Administrator | Budget & Finance |
    When I change the user roles name by "" to work on "Budget & Finance" application module
    Then I should see an error message "User Role Name can't be blank"

  @update_with_duplicate_error_message
  Scenario: Editing the user role information
    And I have the following user role information
      |      name     | application_name |
      | Administrator | Budget & Finance |
    And I have the following user role information
      |      name     | application_name |
      |  Super User   | Budget & Finance |
    When I change the user roles name by "Administrator" to work on "Budget & Finance" application module
    Then I should see an error message "User Role Name has already been taken"

  @list_all_user_roles
  Scenario: List all user roles information
    And I have the following user role information
      |      name     | application_name |
      | Administrator | Budget & Finance |
    And I have the following user role information
      |      name     | application_name |
      |  Super User   | Budget & Finance |
    When I want to see all user role information
    Then I should have the following "2" user role informations
      |      name     | application_name |
      | Administrator | Budget & Finance |
      |  Super User   | Budget & Finance |
