Feature: Login User to the application.
  As a system user
  I want to login into the system
  So that i can access the system activities

  Background:
    Given I have the following application module information
      | application_name | code |
      | Budget & Finance | DUY  |
    And I have the following user information
      | first_name | last_name |    email    |  password  | application_name |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Budget & Finance |

  @login_user
  Scenario: Login user to the application
    When I login to the system with email "sam@abc.com" and password "myPassword"
    Then I should see my full name "Samuel Teshome"

  @login_user_wrong_email
  Scenario: Login unregistered user to the application
    When I login to the system with email "sam1@abc.com" and password "myPassword"
    Then I should see an error message "User doesn't exist or is not allowed!"

  @login_user_wrong_password
  Scenario: Login user with wrong password to the application
    When I login to the system with email "sam@abc.com" and password "mypassword"
    Then I should see an error message "Invalid username or password"
