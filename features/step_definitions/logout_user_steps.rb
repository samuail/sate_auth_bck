Given("I am logged in to the system with email {string} and password {string}") do
|email, password|
  login_path = sate_auth_path + "/login"
  post login_path, :user => { 'email' => email,
                              'password' => password }
end

When("I want to logout of the system") do
  logout_path = sate_auth_path + "/logout"
  @response = get logout_path
end

Then("I should be logged out of the system and see the message {string}") do
|message|
  decoded_response = JSON(@response.body)
  expect(decoded_response["success"]).to eq true
  expect(decoded_response["message"]).to eq message
end
