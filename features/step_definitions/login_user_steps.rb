When("I login to the system with email {string} and password {string}") do |email, password|
  login_path = sate_auth_path + "/login"
  @response = post login_path, :user => { 'email' => email,
                                          'password' => password }
end

Then("I should see my full name {string}") do |full_name|
  @response = JSON(@response.body)
  expect(@response["data"]["full_name"]).to eq full_name
end
