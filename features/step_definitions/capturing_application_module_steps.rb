Given("I am logged in as a super user") do

end

When("I record a application module by the name {string}, and {string} as code") do
|module_name, code|
  app_modules_path = sate_auth_path + "/application_modules"
  @response = post app_modules_path, :application_module => { 'code' => code,
                                                             'name' => module_name }
end

Then("I should have this application module information") do |app_module|
  @response = JSON(@response.body)
  expect(@response["data"]["name"]).to eq app_module.raw[1][0]
  expect(@response["data"]["code"]).to eq app_module.raw[1][1]
end

Then("I should see a success message {string}") do |message|
  expect(@response["message"]).to eq message
end

Then("I should see an error message {string}") do |message|
  @response = JSON(@response.body)
  expect(@response["errors"]).to eq [message]
end

Given("I have the following application module information") do |app_module|
  @appli_module = Sate::Auth::ApplicationModule.create code: app_module.raw[1][1],
                                                       name: app_module.raw[1][0]
end

When("I change the application module by the name {string}, and {string} as code") do
|app_name, app_code|
  app_modules_path = sate_auth_path + "/application_modules/" + @appli_module.to_param
  @response = put app_modules_path, :application_module => { 'code' => app_code,
                                                             'name' => app_name }
end

When("I want to see all applicaion module information") do
  app_modules_path = sate_auth_path + "/application_modules"
  @response = get app_modules_path
end

Then("I should have the following {string} {string} informations") do
|num, model_name, data|
  @response = JSON(@response.body)

  expect(@response["total"]).to eq num.to_i

  if model_name == "application module"
    expect(@response["data"][0]["name"]).to eq data.raw[1][0]
    expect(@response["data"][0]["code"]).to eq data.raw[1][1]

    expect(@response["data"][1]["name"]).to eq data.raw[2][0]
    expect(@response["data"][1]["code"]).to eq data.raw[2][1]
  end
end