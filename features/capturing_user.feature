@user
Feature: Capturing User Information.
  As a system user
  I want to capture user information
  So that user can be able to access the system

  Background:
    Given I am logged in as a super user
    And I have the following application module information
      | application_name | code |
      | Budget & Finance |  BF  |

  @create
  Scenario: Adding the user information
    When I record a user by the first name "Samuel", "Teshome" as a last name, "sam@abc.com" as contact email address, and "myPassword" as password to work on "Budget & Finance" application module
    Then I should have this user information
      | first_name | last_name |    email    |
      | Samuel     | Teshome   | sam@abc.com |
    And I should see a success message "User was successfully created."

  @create_with_error_message
  Scenario: Adding the user information
    When I record a user by the first name "Samuel", "Teshome" as a last name, "" as contact email address, and "myPassword" as password to work on "Budget & Finance" application module
    Then I should see an error message "User Email can't be blank" and "User Email is invalid"

  @update
  Scenario: Editing the user information
    And I have the following user information
      | first_name | last_name |    email    |  password  | application_name |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Budget & Finance |
    When I change the users contact email address by "sami@abc.com"
    Then I should have this user information
      | first_name | last_name |    email     |
      | Samuel     | Teshome   | sami@abc.com |
    And I should see a success message "User was successfully updated."

  @update_with_error_message
  Scenario: Editing the user information
    And I have the following user information
      | first_name | last_name |    email    |  password  | application_name |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Budget & Finance |
    When I change the users contact email address by ""
    Then I should see an error message "User Email can't be blank" and "User Email is invalid"

  @list_all_users
  Scenario: List all user information
    And I have the following user information
      | first_name | last_name |    email    |  password  | application_name |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Budget & Finance |
    And I have the following user information
      | first_name | last_name |    email     |  password  | application_name |
      | Samuel     | Teshome   | sam2@abc.com | myPassword | Budget & Finance |
    When I want to see all user information
    Then I should have the following "2" user informations
      | first_name | last_name |    email     |  password  |
      | Samuel     | Teshome   | sam@abc.com  | myPassword |
      | Samuel     | Teshome   | sam2@abc.com | myPassword |