Rails.application.routes.draw do
  mount Sate::Auth::Engine => '/auth', as: 'sate_auth'
end
