require 'rails_helper'
module Sate::Auth
  RSpec.describe AccessHelper, type: :helper do

    before :each do
      user = build(:sate_auth_user)
      @user_get = Sate::Auth::User.create first_name: user.first_name,
                                         last_name: user.last_name,
                                         email: user.email,
                                         password: user.password,
                                         application_module_id: user.application_module_id
      login_user @user_get
    end

    it "returns the application code" do
      duy_app = Sate::Auth::ApplicationModule.create code: 'DUY',
                                                     name: 'Dummy Application'
      app_name = application_module
      expect(app_name).to eq duy_app
    end

    it "should register the user id into the session record once logged in" do
      expect(session[:user_id]).to eq @user_get.id
    end

    it "should set the current user based upon the session once logged in" do
      expect(current_user).to eq @user_get
    end

    it "should return success if the user is logged in" do
      expect(logged_in?).to eq true
    end

    it "should return success for logged in user" do
      expect(confirm_logged_in).to eq true
    end

    it "should remove session record once the user is logged out" do
      logout_user
      expect(session[:user_id]).not_to eq @user_get.id
      expect(current_user).not_to eq @user_get
    end

    it "should return false for not logged in user" do
      logout_user
      expect(confirm_logged_in).to eq false
    end
  end
end
