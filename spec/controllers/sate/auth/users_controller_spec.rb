require 'rails_helper'

module Sate::Auth
  RSpec.describe UsersController, type: :controller do
    routes { Sate::Auth::Engine.routes }

    before :each do
      @user = build(:sate_auth_user)
    end

    describe "GET #index" do

      before :each do
        @user_get = Sate::Auth::User.create first_name: @user.first_name,
                                            last_name: @user.last_name,
                                            email: @user.email,
                                            password: @user.password,
                                            application_module_id: @user.application_module_id
        get :index
      end

      it "returns a success response" do
        expect(response).to be_successful
      end

      it "assigns all users as @users" do
        expect(assigns(:users)).to eq([@user_get])
      end

    end

    describe "POST #create" do

      email = nil
      let!(:email) { email }

      before :each do
        post :create, params: { user: { 'first_name' => @user.first_name,
                                        'last_name' => @user.last_name,
                                        'email' => email,
                                        'password' => @user.password_digest,
                                        'application_module_id' => @user.application_module_id } }
        @decoded_response = JSON(@response.body)
      end

      context "with valid params" do

        let!(:email) { email = @user.email }

        it "creates a new User" do
          expect(User.count).to eq 1
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "User was successfully created."
        end
      end

      context "with invalid params" do

        let!(:email) { email = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["User Email can't be blank", "User Email is invalid"]
        end
      end
    end

    describe "PUT #update" do

      email = nil
      let!(:email) { email }

      before :each do
        @user_update = create(:sate_auth_user)
        put :update, params: { id: @user_update.to_param,
                               user: { 'first_name' => @user.first_name,
                                       'last_name' => @user.last_name,
                                       'email' => email,
                                       'password' => @user.password_digest,
                                       'application_module_id' => @user.application_module_id } }
        @user_update.reload
        @decoded_response = JSON(@response.body)
      end
      context "with valid params" do

        let!(:email){ email = "new.email@example.com" }

        it "updates the requested user" do
          expect(@user_update.email).to_not eq attributes_for(:sate_auth_user)[:email]
        end

        it "return a success message" do
          expect(@decoded_response["success"]).to eq true
          expect(@decoded_response["message"]).to eq "User was successfully updated."
        end
      end

      context "with invalid params" do

        let!(:email){ email = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["User Email can't be blank", "User Email is invalid"]
        end
      end
    end


  end
end
