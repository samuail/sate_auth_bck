require 'rails_helper'

module Sate::Auth
  RSpec.describe UserRolesController, type: :controller do

    routes { Sate::Auth::Engine.routes }

    before :each do
      @user_role = build(:sate_auth_user_role)
    end

    describe "GET #index" do
      before :each do
        @user_role_get = Sate::Auth::UserRole.create name: @user_role.name,
                                                     application_module_id: @user_role.application_module_id
        get :index
      end

      it "returns a success response" do
        expect(response).to be_successful
      end

      it "assigns all users as @users" do
        expect(assigns(:user_roles)).to eq([@user_role_get])
      end
    end

    describe "POST #create" do
      name = nil
      let!(:name) { name }

      before :each do
        create :sate_auth_user_role, name: "User",
               application_module_id: @user_role.application_module_id
        post :create, params: { user_role: { 'name' => name,
                                             'application_module_id' => @user_role.application_module_id } }
        @decoded_response = JSON(@response.body)
      end
      context "with valid params" do

        let!(:name) { name = @user_role.name }

        it "creates a new UserRole" do
          expect(UserRole.count).to eq 2
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "User role was successfully created."
        end
      end

      context "with invalid params" do
        context "Blank User Role Name" do
          let!(:name){ name = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["User Role Name can't be blank"]
          end
        end

        context "Duplicate User Role Name" do
          let!(:name) { name = "User" }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["User Role Name has already been taken"]
          end
        end
      end
    end

    describe "PUT #update" do
      name = nil
      let!(:name) { name }

      before :each do
        @user_role_update = create(:sate_auth_user_role)
        create :sate_auth_user_role, name: "User",
               application_module_id: @user_role_update.application_module_id
        put :update, params: { id: @user_role_update.to_param,
                               user_role: { 'name' => name,
                                            'application_module_id' => @user_role_update.application_module_id } }
        @user_role_update.reload
        @decoded_response = JSON(@response.body)
      end
      context "with valid params" do
        let!(:name){ name = "Super User" }

        it "updates the requested user_role" do
          expect(@user_role_update.name).to_not eq attributes_for(:sate_auth_user_role)[:name]
        end

        it "return a success message" do
          expect(@decoded_response["success"]).to eq true
          expect(@decoded_response["message"]).to eq "User role was successfully updated."
        end
      end

      context "with invalid params" do
        context "Blank User Role Name" do
          let!(:name){ name = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["User Role Name can't be blank"]
          end
        end

        context "Duplicate User Role Name" do
          let!(:name) { name = "User" }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["User Role Name has already been taken"]
          end
        end
      end

    end

  end
end
