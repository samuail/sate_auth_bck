require 'rails_helper'

module Sate::Auth
  RSpec.describe AccessController, type: :controller do

    routes { Sate::Auth::Engine.routes }

    before :each do
      user = build(:sate_auth_user)
      duy_app = Sate::Auth::ApplicationModule.create code: 'DUY',
                                                     name: 'Dummy Application'
      @user = Sate::Auth::User.create first_name: user.first_name,
                                          last_name: user.last_name,
                                          email: user.email,
                                          password: user.password,
                                          application_module_id: duy_app.id
    end

    describe "POST #login" do
      username = nil
      password = nil
      let!(:email) { username }
      let!(:password) { password }

      before :each do
        post :login, params: { user: { email: username,
                                       password: password } }
        @decoded_response = JSON(@response.body)
      end

      context "with valid params" do

        let!(:email) { username = @user.email }
        let!(:password) { password = @user.password }

        it "login the user to the application" do
          expect(session[:user_id]).to eq @user.id
        end

        it "returns a success and users full name" do
          expect(@response).to be_successful
          expect(@decoded_response["success"]).to eq true
          expect(@decoded_response["data"]["full_name"]).to eq @user.full_name
        end
      end

      context "with invalid params" do
        context "with wrong user name" do
          let!(:email) { username = 'dummy' }
          let!(:password) { password = @user.password }
          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["User doesn't exist or is not allowed!"]
          end
        end

        context "with wrong password" do
          let!(:email) { username = @user.email }
          let!(:password) { password = '123456' }
          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Invalid username or password"]
          end
        end
      end
    end

    describe "GET #logout" do

      before :each do
        post :login, params: { user: { email: @user.email,
                                       password: @user.password } }

        get :logout
        @decoded_response = JSON(@response.body)
      end

      context "with valid params" do
        it "logout the user from the application" do
          expect(session[:user_id]).to eq nil
        end

        it "returns a success and unregisters the user" do
          expect(@response).to be_successful
          expect(@decoded_response["success"]).to eq true
          expect(@decoded_response["message"]).to eq "User Successfully logged out."
        end
      end
    end
  end
end
