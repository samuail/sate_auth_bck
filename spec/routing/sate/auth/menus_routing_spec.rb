require "rails_helper"

module Sate::Auth
  RSpec.describe MenusController, type: :routing do
    describe "routing" do

      routes { Sate::Auth::Engine.routes }

      it "routes to #index" do
        expect(:get => "/menus").to route_to("sate/auth/menus#index")
      end

      it "routes to #create" do
        expect(:post => "/menus").to route_to("sate/auth/menus#create")
      end

      it "routes to #update via PUT" do
        expect(:put => "/menus/1").to route_to("sate/auth/menus#update", :id => "1")
      end

      it "routes to #update via PATCH" do
        expect(:patch => "/menus/1").to route_to("sate/auth/menus#update", :id => "1")
      end

    end
  end
end
