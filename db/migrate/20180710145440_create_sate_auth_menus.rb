class CreateSateAuthMenus < ActiveRecord::Migration[5.2]
  def change
    create_table :sate_auth_menus do |t|
      t.string :text, limit: 50, null: false
      t.string :icon_cls
      t.string :class_name
      t.string :location
      t.integer :parent_id, index: true
      t.references :application_module, index: true

      t.timestamps
    end

    add_index :sate_auth_menus, [:text, :application_module_id], :unique => true
    add_foreign_key :sate_auth_menus, :sate_auth_menus, :column => :parent_id
    add_foreign_key :sate_auth_menus, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
