module Sate::Auth
  # This is a helper class to manage access controls
  module AccessHelper
    def application_module
      code = Rails.configuration.x.application_module_code
      ApplicationModule.find_by_code code
    end

    def login_user user
      session[:user_id] = user.id
    end

    def logged_in?
      !current_user.nil?
    end

    def current_user
      @current_user ||= User.find_by_id session[:user_id]
    end

    def logout_user
      session.delete(:user_id)
      @current_user = nil
    end

    private

    def confirm_logged_in
      if session[:user_id]
        true
      else
        false
      end
    end

  end
end
