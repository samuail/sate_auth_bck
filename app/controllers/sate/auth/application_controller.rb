require 'sate/common/methodresponse'
require 'sate/common/util'
module Sate
  module Auth
    class ApplicationController < ActionController::Base
      include AccessHelper
      protect_from_forgery prepend: true

      before_action :allow_cross_domain_ajax
      def allow_cross_domain_ajax
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Request-Method'] = 'GET, POST, OPTIONS'
      end
    end
  end
end
