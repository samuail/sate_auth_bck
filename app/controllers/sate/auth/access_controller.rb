require_dependency "sate/auth/application_controller"

module Sate::Auth
  class AccessController < ApplicationController
    before_action :confirm_logged_in, :except => [:login]
    def login
      app_module = application_module
      user = User.find_by_email params[:user][:email].downcase

      if user && user.application_module_id == app_module.id
        if user.authenticate(params[:user][:password])
          login_user user
          response = Sate::Common::MethodResponse.new(true,
                                                      nil,
                                                      {'full_name' => user.full_name},
                                                      nil,
                                                      1)
        else
          errors = ["Invalid username or password"]
          response = Sate::Common::MethodResponse.new(false,
                                                      nil,
                                                      nil,
                                                      errors,
                                                      0)
        end
      else
        errors = ["User doesn't exist or is not allowed!"]
        response = Sate::Common::MethodResponse.new(false,
                                                    nil,
                                                    nil,
                                                    errors,
                                                    0)
      end
      render json: response
    end

    def logout
      logout_user if logged_in?
      response = Sate::Common::MethodResponse.new(true,
                                                  "User Successfully logged out.",
                                                  nil,
                                                  nil,
                                                  0)
      render json: response
    end
  end
end
