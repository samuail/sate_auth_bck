require_dependency "sate/auth/application_controller"

module Sate::Auth
  class ApplicationModulesController < ApplicationController
    before_action :set_application_module, only: [:update]

    # GET /application_modules
    def index
      @application_modules = ApplicationModule.all
      total = @application_modules.count
      response = Sate::Common::MethodResponse.new(true, nil, @application_modules, nil, total)
      render json: response
    end

    # POST /application_modules
    def create
      @application_module = ApplicationModule.new(application_module_params)

      if @application_module.save
        response = Sate::Common::MethodResponse.new(
            true, 'Application module was successfully created.', @application_module, nil, 0)
      else
        errors = Sate::Common::Util.error_messages @application_module,
                                                   'Application Module'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
      render json: response
    end

    # PATCH/PUT /application_modules/1
    def update
      if @application_module.update(application_module_params)
        response = Sate::Common::MethodResponse.new(
            true, 'Application module was successfully updated.', @application_module, nil, 0)
      else
        errors = Sate::Common::Util.error_messages @application_module,
                                                   'Application Module'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
      render json: response
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_application_module
        @application_module = ApplicationModule.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def application_module_params
        params.require(:application_module).permit(:code, :name)
      end
  end
end
