Sate::Auth::Engine.routes.draw do
  post '/login', to: 'access#login'

  get '/logout', to: 'access#logout'

  resources :application_modules
  resources :users
  resources :user_roles
  resources :menus
end
